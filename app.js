	

	var app = new Vue({
		el:"#root",
		data:{
			showAddModal: false,
			showeditModal: false,
			deleteModal: false,
			showErrMessage:"",
			showSuccMessage:"",
			users:[],
			newUser:{name:"",email:"",mobile:""},
			clickedUser:{}
		},
		mounted:function(){
			this.getUser();
		},
		methods:{
			getUser:function(){
				axios.get("http://localhost/phpandVue/api.php?action=read").then(function(response){
					console.log(response);
					if(response.data.error){
						app.showErrMessage = response.data.message;
					}else{
						app.users = response.data.users;
					}
				});
			},
			addUser:function(){
				var formData = app.toFormData(app.newUser);
				axios.post("http://localhost/phpandVue/api.php?action=create",formData).then(function(response){
					app.newUser = {name:"",email:"",mobile:""};
					if(response.data.error){
						app.showErrMessage = response.data.message;
					}else{
						app.showSuccMessage = response.data.message;
						app.getUser();
					}
				});
			},
			updateUser:function(){
				var formData = app.toFormData(app.clickedUser);
				axios.post("http://localhost/phpandVue/api.php?action=update",formData).then(function(response){
					app.clickedUser = {};
					if(response.data.error){
						app.showErrMessage = response.data.message;
					}else{
						app.showSuccMessage = response.data.message;
						app.getUser();
					}
				});
			},
			deleteUser:function(){
				var formData = app.toFormData(app.clickedUser);
				axios.post("http://localhost/phpandVue/api.php?action=delete",formData).then(function(response){
					app.clickedUser = {};
					if(response.data.error){
						app.showErrMessage = response.data.message;
					}else{
						app.showSuccMessage = response.data.message;
						app.getUser();
					}
				});
			},
			selectUser:function(user){
				app.clickedUser = user;
			},
			toFormData:function(obj){
				var form_data = new FormData();
				for(var key in obj){
					form_data.append(key,obj[key]);
				}
				return form_data
			}
		}
	});