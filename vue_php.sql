-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 10, 2018 at 06:36 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.9-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vue_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(299) NOT NULL,
  `description` varchar(299) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`) VALUES
(1, 'nokia', 'nokia'),
(2, 'Samsung', 'samasung'),
(3, 'Iphone', 'Iphone'),
(5, 'Kuddus', 'kuddus');

-- --------------------------------------------------------

--
-- Table structure for table `logUser`
--

CREATE TABLE `logUser` (
  `id` int(11) NOT NULL,
  `user_name` varchar(299) NOT NULL,
  `token` varchar(299) NOT NULL,
  `password` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logUser`
--

INSERT INTO `logUser` (`id`, `user_name`, `token`, `password`) VALUES
(1, 'Mkuddus', 'fqD5CnZdHK6or7ZJZtsEpDHaGq2wBHFh+3g/PH/TYGptDhVDjlJH3zPC9cwm/fjuhdSRd7iztKffppPsbau+sw==', '81dc9bdb52d04dc20036dbd8313ed055'),
(2, 'hello', 'fHcfXimg6Q2IfGk3LJZdjWsFj9F01VqlQql0DiuvtmtbIzqrts9u4UKpih8A0YsM5jSqgNM9Ygr1bnPRocke0Q==', '733d7be2196ff70efaf6913fc8bdcabf');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_category` varchar(20) NOT NULL,
  `product_supplier` varchar(20) NOT NULL,
  `product_price` varchar(10) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `product_description` varchar(299) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `id` int(11) NOT NULL,
  `image_name` varchar(299) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `image_name`) VALUES
(1, 'Images/d2b50c64c8.png'),
(2, 'Images/861b5bac52.png'),
(3, 'uploads/e5584ccdbc.png'),
(4, 'uploads/fca9c6a2c5.png'),
(5, 'uploads/ad83193d68.png'),
(6, 'uploads/8f388e42d2.png'),
(7, 'uploads/89d9bcb8a4.png'),
(8, 'uploads/b581a0398d.png'),
(9, 'uploads/default.png'),
(10, 'uploads/a0b31958a7.png'),
(11, 'uploads/6acfc35955.png'),
(12, 'uploads/d899d900f2.jpg'),
(13, 'uploads/44f92205fb.jpg'),
(14, 'uploads/dd310d566b.jpg'),
(15, 'uploads/02e01e6637.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(299) NOT NULL,
  `description` varchar(299) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `description`) VALUES
(1, 'Kuddus', 'kuddus');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(99) NOT NULL,
  `email` varchar(99) NOT NULL,
  `mobile` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`) VALUES
(1, 'kuddus', 'kuddus@gmail.com ', '454455445'),
(19, 'fgfg', 'fgfg', 'fg'),
(20, 'Mkuddus', 'email@emal.com', 'jjdfjdsafj');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logUser`
--
ALTER TABLE `logUser`
  ADD PRIMARY KEY (`id`) USING HASH,
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING HASH,
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `logUser`
--
ALTER TABLE `logUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
